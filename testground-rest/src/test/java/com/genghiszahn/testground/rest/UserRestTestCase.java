package com.genghiszahn.testground.rest;

import java.util.HashMap;
import java.util.Map;

import org.apache.http.HttpResponse;

import com.genghiszahn.testground.model.User;

public class UserRestTestCase extends RestTestCase {
    static final String SERVICE_URI = "user";

    public void testCheckLogin() throws Exception {
        final String assertionEmail = "test.user@genghiszahn.com";

        Map<String,String> paramMap = new HashMap<String,String>();
        paramMap.put("userName", "testuser");
        paramMap.put("password", "unhashed");
        HttpResponse response = postForm(SERVICE_URI+"/login", paramMap);
        
        assertOk(response);
        User user = jsonMapper
                .readValue(response.getEntity().getContent(), User.class);
        assertNotNull(user);
        assertEquals(assertionEmail, user.getEmailAddress());
        
    }
}
