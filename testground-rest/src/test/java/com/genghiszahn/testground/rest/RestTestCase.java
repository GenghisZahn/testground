package com.genghiszahn.testground.rest;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.ws.rs.core.MediaType;

import junit.framework.TestCase;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * 
 * @author zahnant
 *
 */
public abstract class RestTestCase extends TestCase {

    /** Server port number. */
    public static final int TEST_SERVER_PORT = 29090;
    /** Path to rest service. */
    public static final String TEST_SERVER_PATH = 
            "http://localhost:" + TEST_SERVER_PORT + "/testground-rest/rest/"; 
    
    /** JSON to java object mapper. */
    protected static ObjectMapper jsonMapper = new ObjectMapper();
    
    /** Apache HTTP client. */
    private CloseableHttpClient httpClient = HttpClients.createDefault();
    
    /** Request configuration. */ 
    private RequestConfig requestConfig;
    
    @Override    
    public void setUp() throws ClientProtocolException, IOException {
        final int timeoutMs = 60000; 
        RequestConfig.Builder configBuilder = RequestConfig.custom();
        configBuilder.setConnectionRequestTimeout(timeoutMs);
        requestConfig = configBuilder.build();
        
    }
          
    /**
     * Perform an HTTP GET and return the response.
     * 
     * @param uri The relative URI to retrieve.
     * @return HttpResponse
     * @throws ClientProtocolException
     * @throws IOException
     */
    protected HttpResponse get(String uri) 
            throws ClientProtocolException, IOException {

        System.out.println("GET " + uri);
        
        HttpGet getRequest = new HttpGet(TEST_SERVER_PATH + uri);
        getRequest.setConfig(requestConfig);
        CloseableHttpResponse response = httpClient.execute(getRequest);
        
        return response;
    }

    /** 
     * Perform an HTTP POST and return the response.
     * 
     * @param uri The relative URI to retrieve.
     * @return HttpResponse
     * @throws ClientProtocolException
     * @throws IOException
     */
    protected HttpResponse postJson(String uri, String json) 
            throws ClientProtocolException, IOException {

        System.out.println("POST " + uri);

        HttpPost postRequest = new HttpPost(TEST_SERVER_PATH + uri);
        postRequest.setConfig(requestConfig);
        
        if (json != null && !json.isEmpty()) {
            StringEntity input = new StringEntity(json);
            input.setContentType(MediaType.APPLICATION_JSON);
            postRequest.setEntity(input);
        }
        CloseableHttpResponse response = httpClient.execute(postRequest);
        
        return response;
    }

    /**
     * 
     * @param uri
     * @param paramMap
     * @return
     * @throws ClientProtocolException
     * @throws IOException
     */
    protected HttpResponse postForm(String uri, Map<String,String> paramMap) 
            throws ClientProtocolException, IOException {
        System.out.println("POST " + uri);

        HttpPost postRequest = new HttpPost(TEST_SERVER_PATH + uri);
        postRequest.setConfig(requestConfig);
        
        if (paramMap != null && !paramMap.isEmpty()) {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            
            for(String key : paramMap.keySet()) {
                params.add( new BasicNameValuePair(key, paramMap.get(key)));
            }
//            input.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
            postRequest.setEntity(new UrlEncodedFormEntity(params));
        }
        CloseableHttpResponse response = httpClient.execute(postRequest);
        
        return response;
        
    }

    /**
     * Assert that the response status code is in the 200 (success) range. 
     * @param response HttpResponse object to check status code for. 
     */
    protected void assertOk(HttpResponse response) {
        assertNotNull("Null HttpResponse object encountered.", response);
        assertNotNull("Response status line is null.", response.getStatusLine());
        int statusCode = response.getStatusLine().getStatusCode();
        assertTrue("Encountered HTTP response code " + statusCode, 
                statusCode >= 200 && statusCode <= 206);
    }
    
    /**
     * @param <E> Return object type.
     * @param response HttpResponse
     * @return E
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws IllegalStateException
     * @throws IOException
     */
    protected static<E> E getJsonObject(HttpResponse response) 
    throws JsonParseException, JsonMappingException, IllegalStateException, IOException {
        E jsonObj = jsonMapper.readValue(response.getEntity().getContent(), 
                new TypeReference<E>() { });
        
        return jsonObj;
    }
}