package com.genghiszahn.testground.websocket;

import java.net.URI;

import javax.websocket.ContainerProvider;
import javax.websocket.Session;
import javax.websocket.WebSocketContainer;

import junit.framework.TestCase;

public class ChatWebSocketTestCase extends TestCase {
//    final private Logger logger = LoggerFactory.getLogger(ChatWebSocketTestCase.class);

    /** Server port number. */
    public static final int TEST_SERVER_PORT = 29090;
    /** Path to rest service. */
    public static final String TEST_SERVER_PATH = 
            "ws://localhost:" + TEST_SERVER_PORT + "/testground-rest/chat"; 

    public void testChatWebSocket() throws Exception {
        WebSocketContainer container = ContainerProvider.getWebSocketContainer();
        Session session = container.connectToServer(ChatWebSocketClient.class, URI.create(TEST_SERVER_PATH));
        
        assertNotNull(session);
        assertTrue(session.isOpen());
        
        // This can be improved upon, 
        // for now we must keep the test case open until the session is closed to avoid an error.
        while(session.isOpen()) {
        }
    }
}
