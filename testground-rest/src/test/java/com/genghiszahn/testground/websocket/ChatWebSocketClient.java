package com.genghiszahn.testground.websocket;

import java.io.IOException;

import javax.websocket.ClientEndpoint;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@ClientEndpoint
public class ChatWebSocketClient {
    final private Logger logger = LoggerFactory.getLogger(ChatWebSocketClient.class);
    final static String QUIT_MESSAGE = "/quit";
    
    @OnOpen
    public void onOpen(Session session) {
        final String message = "Hello world!";
        logger.info("Sending message to chat endpoint: " + message);
        try {
            session.getBasicRemote().sendText(message);
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
        }
    }
    
    @OnMessage
    public void onMessage(String message, Session session) {
        logger.info("Message from server:" + message);
        try {
            session.getBasicRemote().sendText(QUIT_MESSAGE);
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
        }        
    }
}
