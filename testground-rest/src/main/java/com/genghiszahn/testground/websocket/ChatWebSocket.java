package com.genghiszahn.testground.websocket;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.websocket.CloseReason;
import javax.websocket.CloseReason.CloseCodes;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@ServerEndpoint(value = "/chat")
public class ChatWebSocket {
    private final Logger logger = LoggerFactory.getLogger(ChatWebSocket.class);
    private static List<Session> sessions = new ArrayList<Session>();
    
    public ChatWebSocket() {
        logger.info("Initializing Chat WebSocket.");        
    }
    
    @OnOpen
    public void onOpen(Session session) {
        logger.info("Connected. Session ID: " + session.getId());
        ChatWebSocket.sessions.add(session);
    }
 
    @OnMessage
    public void onMessage(String message, Session session) {

        switch (message) {
            case "/quit":
                try {
                    session.close(new CloseReason(CloseCodes.NORMAL_CLOSURE, "Session ended"));
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
                break;
                
            default:
                for (Session currSession : ChatWebSocket.sessions) {
                    try {
                        currSession.getBasicRemote().sendText(message);
                    } catch (IOException e) {
                        // ignore
                    }
                }
        }
    }
 
    @OnClose
    public void onClose(Session session, CloseReason closeReason) {
        ChatWebSocket.sessions.remove(session);
        logger.info(String.format("Session %s closed because of %s", session.getId(), closeReason));
    }
}
