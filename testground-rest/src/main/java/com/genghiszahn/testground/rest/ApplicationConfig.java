package com.genghiszahn.testground.rest;

import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.web.filter.RequestContextFilter;

public class ApplicationConfig extends ResourceConfig {

    /**
     * 
     */
    public ApplicationConfig() {
        // for Spring integration
        register(RequestContextFilter.class);
        // for JSON
        register(JacksonFeature.class);
        
        // REST classes
        register(UserRest.class);
    }
}
