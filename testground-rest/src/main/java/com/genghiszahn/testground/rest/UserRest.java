package com.genghiszahn.testground.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.genghiszahn.testground.model.User;
import com.genghiszahn.testground.services.UserService;

@Path("/user")
@Produces(MediaType.APPLICATION_JSON)
public class UserRest {
	private static UserService userService;

	public void setUserService(UserService userService) {
	    
	    System.out.println("Setting userService");
		UserRest.userService = userService;
	}
	
	@POST
	@Path("/login")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public User checkLogin(@FormParam("userName") String userName, 
						   @FormParam("password") String password) {
	    
		return UserRest.userService.checkLogin(userName, password);
	}
}
