package com.genghiszahn.testground.services;

import java.util.List;

import com.genghiszahn.testground.model.ItemCollection;

public interface CollectionService {
    /**
     * Retrieve a list of all collections available to the user.
     * For performance, these collections will not have their item lists populated.
     * 
     * @param userId Unique ID of user performing action, for access control
     * @return List of ItemCollection objects.
     */
    public List<ItemCollection> getCollectionsForUser(int userId);
    
    /**
     * Retrieve a collection from the database.
     * 
     * @param userId Unique ID of user performing action, for access control
     * @param collectionId Unit ID of collection to retrieve.
     * @return ItemCollection object.
     */
    ItemCollection getCollection(int userId, int collectionId);
    
    /**
     * Persists collection data, delegating between create or update 
     * based on the existence of a collection Id.
     * 
     * @param userId Unique ID of user performing action, for access control
     * @param collection ItemCollection object to persist.
     */
    void saveCollection(int userId, ItemCollection collection);
    
    /**
     * Remove a collection from the database.
     * 
     * @param userId Unique ID of user performing action, for access control
     * @param collectionId Unique ID of collection to delete.
     */
    void deleteCollection(int userId, int collectionId);    
}
