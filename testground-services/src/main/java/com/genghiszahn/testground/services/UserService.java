package com.genghiszahn.testground.services;

import com.genghiszahn.testground.model.User;

public interface UserService {
    /**
     * Returns null if username does not exist or password is invalid.
     * @param userLoginName
     * @param password
     * @return
     */
    User checkLogin(String userLoginName, String password);
}
