package com.genghiszahn.testground.common.codegen;

import java.util.Arrays;
import java.util.List;

import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTParser;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.FieldDeclaration;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.TypeDeclaration;
import org.eclipse.jdt.core.dom.VariableDeclarationFragment;
import org.jibx.binding.model.ElementBase;
import org.jibx.schema.codegen.EnumerationClassHolder;
import org.jibx.schema.codegen.IClassHolder;
import org.jibx.schema.codegen.extend.ClassDecorator;

public class EqualsMethodDecorator implements ClassDecorator {
    private String equalsMethodTemplate = "class dummy {"
          + "    @Override\n"
          + "    public boolean equals(Object obj) {\n"
          + "        if (this == obj)\n"
          + "            return true;\n"
          + "        if (obj == null)\n"
          + "            return false;\n"
          + "        if (getClass() != obj.getClass())\n"
          + "            return false;\n"
          + "        ${className} other = (${className}) obj;\n";

    private String templateEnd = 
            "        return true;\n"
          + "    }}";
    
    private String complexFieldCompareTemplate = 
            "    if (${fieldName} == null) {\n"
          + "        if (other.${fieldName} != null)\n"
          + "            return false;\n"
          + "    } else if (!${fieldName}.equals(other.${fieldName}))\n"
          + "        return false;\n";

    private String primitiveFieldCompareTemplate = 
            "    if (${fieldName} != other.${fieldName})\n"
          + "        return false;\n";

    private String arrayFieldCompareTemplate = 
            "    if (!Arrays.equals(${fieldName}, other.${fieldName}))\n"
          + "        return false;\n";
    
    /** Parser instance used by class. */
    private final ASTParser parser = ASTParser.newParser(AST.JLS3);
    
    @Override
    public void start(IClassHolder holder) { }

    @Override
    public void valueAdded(String basename, boolean collect, String type,
            FieldDeclaration field, MethodDeclaration getmeth,
            MethodDeclaration setmeth, String descript, IClassHolder holder) { }

    @Override
    public void finish(ElementBase binding, IClassHolder holder) {
        if (holder instanceof EnumerationClassHolder) {
            return;
        }
        
        StringBuffer buff = new StringBuffer(equalsMethodTemplate);
        replace("${className}", holder.getName(), buff);
        for (FieldDeclaration field : Arrays.asList(holder.getFields())) {
            VariableDeclarationFragment vardecl = (VariableDeclarationFragment)field.fragments().get(0);
            String fieldName = vardecl.getName().getIdentifier();

            if (field.getType().isArrayType()) {
                holder.addImport("java.util.Arrays");
                buff.append(arrayFieldCompareTemplate);
                replace("${fieldName}", fieldName, buff);

            } else if (field.getType().isPrimitiveType()) {
                buff.append(primitiveFieldCompareTemplate);
                replace("${fieldName}", fieldName, buff);

            } else {
                buff.append(complexFieldCompareTemplate);
                replace("${fieldName}", fieldName, buff);
            }
        }
        buff.append(templateEnd);
        
        // parse the resulting text
        parser.setSource(buff.toString().toCharArray());
        CompilationUnit unit = (CompilationUnit)parser.createAST(null);
        //            
        // add all methods from output tree to class under construction
        TypeDeclaration typedecl = (TypeDeclaration)unit.types().get(0);
        for (ASTNode node : (List<ASTNode>) typedecl.bodyDeclarations()) {
            if (node instanceof MethodDeclaration) {
                holder.addMethod((MethodDeclaration)node);
            }
        }
    }
    
    /**
     * Replace all occurrences of one string with another in a buffer.
     *
     * @param match
     * @param replace
     * @param buff
     */
    private void replace(String match, String replace, StringBuffer buff) {
        int base = 0;
        while ((base = buff.indexOf(match, base)) >= 0) {
            buff.replace(base, base+match.length(), replace);
        }
    }
}
