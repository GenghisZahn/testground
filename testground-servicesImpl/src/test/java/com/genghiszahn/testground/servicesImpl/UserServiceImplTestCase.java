package com.genghiszahn.testground.servicesImpl;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.genghiszahn.testground.dao.UserDao;
import com.genghiszahn.testground.model.User;
import com.genghiszahn.testground.services.UserService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:springContext/test-testground-servicesImpl-context.xml",
								 "classpath:springContext/testground-servicesImpl-context.xml"})	
public class UserServiceImplTestCase {
    @Autowired
    private UserDao mockUserDao;
    
    @Autowired
    private UserService userService;
    
    @Before
    public void setUp() throws Exception {
        Mockito.reset(mockUserDao);
    }

    @Test
    public void testCheckLogin() throws Exception {
        final int userId = 1;
        final String userLoginName = "testuser";
        final String userEmailAddress = "test.user@genghiszahn.com"; 
        final String password = "unhashed";
        // hash using bcrypt
        final String passwordHash = BCrypt.hashpw(password, BCrypt.gensalt());
        boolean isGood = BCrypt.checkpw(password, passwordHash);
        System.out.println("PW: " + password + " hash: " + passwordHash + " isGood: " + isGood);
        
        User mockUser = new User();
        mockUser.setId(userId);
        mockUser.setLoginName(userLoginName);
        mockUser.setPasswordHash(passwordHash);
        mockUser.setEmailAddress(userEmailAddress);
        
        Mockito.when(this.mockUserDao.getUserByLoginName(userLoginName)).thenReturn(mockUser);

        User user = this.userService.checkLogin(userLoginName, password);
        assertEquals(mockUser, user);
        
        Mockito.verify(this.mockUserDao).getUserByLoginName(userLoginName);
    }
}
