package com.genghiszahn.testground.servicesImpl;

import org.springframework.security.crypto.bcrypt.BCrypt;

import com.genghiszahn.testground.dao.UserDao;
import com.genghiszahn.testground.model.User;
import com.genghiszahn.testground.services.UserService;

public class UserServiceImpl implements UserService {
    private UserDao userDao;

    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }
    
    public User checkLogin(String userLoginName, String password) {
        User user = userDao.getUserByLoginName(userLoginName);
        
        if( user != null && user.getPasswordHash() != null && 
            BCrypt.checkpw(password, user.getPasswordHash()) ) {
            return user;
        }
        return null;
    }
}
