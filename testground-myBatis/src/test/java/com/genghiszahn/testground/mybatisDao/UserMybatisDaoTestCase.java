package com.genghiszahn.testground.mybatisDao;

import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.genghiszahn.testground.dao.UserDao;
import com.genghiszahn.testground.model.User;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:springContext/test-testground-mybatis-context.xml",
								 "classpath:springContext/testground-mybatis-context.xml"})
public class UserMybatisDaoTestCase {
    @Autowired
    private UserDao userDao;
    
    @Test
    public void testRetrieveUser() {
        final int userId = 1;
        final String userLoginName = "testuser";
        final String emailAddress = "test.user@genghiszahn.com";
        
        User user = this.userDao.retrieveUser(userId);
        assertNotNull(user);
        assertEquals(user.getLoginName(), userLoginName);
        assertEquals(user.getEmailAddress(), emailAddress);
    }

}
