package com.genghiszahn.testground.mybatisDao;

import java.util.List;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.genghiszahn.testground.dao.ItemDao;
import com.genghiszahn.testground.model.Item;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:springContext/test-testground-mybatis-context.xml",
								 "classpath:springContext/testground-mybatis-context.xml"})
public class ItemMybatisDaoTestCase {
    @Autowired
    private ItemDao itemDao;
    
    @Test
    public void testCreateItem() {
        Item newItem = new Item();
        newItem.setName("Behemoth");
        
        this.itemDao.createItem(newItem);
        assertNotNull(newItem);
        assertNotNull(newItem.getId());
    }
    
    @Test
    public void testGetUserById() {
        final int userId = 1;

        List<Item> items = this.itemDao.getItemsForUser(userId);
        assertNotNull(items);
        for(Item item : items) {
            assertNotNull(item);
            assertNotNull(item.getName());
            assertFalse(item.getId() == 7);
        }
    }

}
