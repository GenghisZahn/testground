package com.genghiszahn.testground.mybatisDao;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.genghiszahn.testground.dao.CollectionDao;
import com.genghiszahn.testground.model.AccessType;
import com.genghiszahn.testground.model.Item;
import com.genghiszahn.testground.model.ItemCollection;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:springContext/test-testground-mybatis-context.xml",
								 "classpath:springContext/testground-mybatis-context.xml"})
public class CollectionMybatisDaoTestCase {
    
    @Autowired
    private CollectionDao collectionDao;
    
    @Test
    public void testGetCollectionsForUser() {        
        final int userId = 1;
        final String expectedCollectionName = "Khador";
        
        assertNotNull(this.collectionDao);
        
        List<ItemCollection> collections = this.collectionDao.getCollectionsForUser(userId);
        assertNotNull(collections);
        assertEquals(1, collections.size());
        
        assertEquals(expectedCollectionName, collections.get(0).getName());
    }
    
    @Test
    public void testRetrieveCollection() {        
        final int userId = 1;
        final int collectionId = 1;
        final String expectedCollectionName = "Khador";
        final int expectedItemCount = 6;
        
        assertNotNull(this.collectionDao);
        
        ItemCollection collection = this.collectionDao.retrieveCollection(userId, collectionId);
        assertNotNull(collection);
        
        assertEquals(expectedCollectionName, collection.getName());
        assertNotNull(collection.getItemList());
        assertEquals(expectedItemCount, collection.getItemList().size());
    }
    
    @Test
    public void testGroup_createUpdateDelete() {
        final int userId = 1;
        assertNotNull(this.collectionDao);

        int collectionId = testCreateCollection(userId);
        testUpdateCollection(userId, collectionId);
        testDeleteCollection(userId, collectionId);
    }
    
    private int testCreateCollection(int userId) {
        ItemCollection newCollection = new ItemCollection();
        newCollection.setName("Test Collection");
        newCollection.setDescription("Test Description.");
        newCollection.setAccessType(AccessType.READ_WRITE);
        Item item1 = new Item();
        item1.setId(1);
        List<Item> itemList = new ArrayList<Item>();
        itemList.add(item1);
        newCollection.setItemList(itemList);
        
        this.collectionDao.createCollection(userId, newCollection);        
        assertNotNull(newCollection.getId());
        System.out.println("Generated collection id:" + newCollection.getId());
        
        ItemCollection checkCollection = this.collectionDao.retrieveCollection(userId, newCollection.getId());
        assertNotNull(checkCollection);
        assertNotNull(checkCollection.getItemList());
        assertEquals(itemList.size(), checkCollection.getItemList().size());
        
        return newCollection.getId();
    }
    
    private void testUpdateCollection(int userId, int collectionId) {
        ItemCollection updatedCollection = new ItemCollection();
        updatedCollection.setId(collectionId);
        updatedCollection.setName("Updated Test Collection");
        updatedCollection.setDescription("Updated Test Description.");
        updatedCollection.setAccessType(AccessType.READ_WRITE);
        List<Item> itemList = new ArrayList<Item>();
        Item item1 = new Item();
        item1.setId(1);
        Item item2 = new Item();
        item2.setId(2);
        itemList.add(item2);
        updatedCollection.setItemList(itemList);
        
        this.collectionDao.updateCollection(userId, updatedCollection);
        
        ItemCollection checkCollection = this.collectionDao.retrieveCollection(userId, collectionId);
        assertNotNull(checkCollection);
        assertNotNull(checkCollection.getItemList());
        assertEquals(itemList.size(), checkCollection.getItemList().size());
        
    }

    private void testDeleteCollection(int userId, int collectionId) {
        this.collectionDao.deleteCollection(userId, collectionId);
        
        assertNull(this.collectionDao.retrieveCollection(userId, collectionId));
    }
    
}
