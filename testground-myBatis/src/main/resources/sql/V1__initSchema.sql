-- ---
-- Table 'users'
-- 
-- ---

DROP TABLE IF EXISTS users CASCADE;
DROP TABLE IF EXISTS accounts CASCADE;
DROP TABLE IF EXISTS collections CASCADE;
DROP TABLE IF EXISTS collections_items CASCADE;
DROP TABLE IF EXISTS ref_permissions CASCADE;
DROP TABLE IF EXISTS items CASCADE;
        
CREATE TABLE users (
  user_id SERIAL primary key,
  login_name VARCHAR(140) NOT NULL UNIQUE,
  password_hash VARCHAR(255) NOT NULL,
  user_email VARCHAR(255) NOT NULL UNIQUE
);

-- ---
-- Table 'accounts'
-- Links users and collections
-- ---
        
CREATE TABLE accounts (
  user_id SERIAL,
  collection_id INTEGER NOT NULL,
  permission_code CHAR(10) NOT NULL,
  UNIQUE (user_id, collection_id)
);

-- ---
-- Table 'collections'
-- 
-- ---
        
CREATE TABLE collections (
  collection_id SERIAL primary key,
  collection_name VARCHAR(140) NOT NULL,
  collection_description VARCHAR(1000)
);

-- ---
-- Table 'collections_items'
-- 
-- ---
        
CREATE TABLE collections_items (
  collection_id INTEGER NOT NULL,
  item_id INTEGER NOT NULL,
  quantity INTEGER NOT NULL DEFAULT 1,
  UNIQUE (collection_id, item_id)
);

-- ---
-- Table 'ref_permissions'
-- 
-- ---
       
CREATE TABLE ref_permissions (
  permission_code VARCHAR(20) NOT NULL primary key,
  description VARCHAR(1000) DEFAULT NULL
);

-- ---
-- Table 'items'
-- 
-- ---
        
CREATE TABLE items (
  item_id SERIAL primary key,
  item_name VARCHAR(140) NOT NULL,
  item_description VARCHAR(1000) DEFAULT NULL,
  image_uri VARCHAR(200) DEFAULT NULL
);

-- ---
-- Foreign Keys 
-- ---

ALTER TABLE accounts ADD FOREIGN KEY (user_id) REFERENCES users (user_id);
ALTER TABLE accounts ADD FOREIGN KEY (collection_id) REFERENCES collections (collection_id);
ALTER TABLE accounts ADD FOREIGN KEY (permission_code) REFERENCES ref_permissions (permission_code);
ALTER TABLE collections_items ADD FOREIGN KEY (collection_id) REFERENCES collections (collection_id);
ALTER TABLE collections_items ADD FOREIGN KEY (item_id) REFERENCES items (item_id);

-- ---
-- Reference Data
-- ---
 INSERT INTO ref_permissions (permission_code,Description) VALUES ('READ_ONLY','Read only permission.');
 INSERT INTO ref_permissions (permission_code,Description) VALUES ('READ_WRITE','Read and write permissions.');

-- ---
-- Test Data
-- ---

 INSERT INTO users (login_name,password_hash,user_email) VALUES
 ('testuser','$2a$10$plxI8yydDzqgelgYDBMNPeGAErBWj/LCm/LTTZsMu.dJ/qvS1mEWq','test.user@genghiszahn.com');
 
 INSERT INTO items (item_name,item_description,image_uri) VALUES ('Forward Kommander Sorscha','Sorscha1',null);
 INSERT INTO items (item_name,item_description,image_uri) VALUES ('The Butcher of Khardov','Butcher1',null);
 INSERT INTO items (item_name,item_description,image_uri) VALUES ('Vladimir Tzepesci, The Dark Prince','Vlad1',null);
 INSERT INTO items (item_name,item_description,image_uri) VALUES ('Juggernaut',null,null);
 INSERT INTO items (item_name,item_description,image_uri) VALUES ('Destroyer',null,null);
 INSERT INTO items (item_name,item_description,image_uri) VALUES ('Marauder',null,null);
 INSERT INTO items (item_name,item_description,image_uri) VALUES ('Decimator',null,null);

 INSERT INTO collections (collection_name) VALUES ('Khador');

 INSERT INTO collections_items (collection_id,item_id,quantity) VALUES (1,1,1);
 INSERT INTO collections_items (collection_id,item_id,quantity) VALUES (1,2,1);
 INSERT INTO collections_items (collection_id,item_id,quantity) VALUES (1,3,1);
 INSERT INTO collections_items (collection_id,item_id,quantity) VALUES (1,4,1);
 INSERT INTO collections_items (collection_id,item_id,quantity) VALUES (1,5,1);
 INSERT INTO collections_items (collection_id,item_id,quantity) VALUES (1,6,1);

 INSERT INTO accounts (user_id,collection_id,permission_code) VALUES (1,1,'READ_WRITE');

