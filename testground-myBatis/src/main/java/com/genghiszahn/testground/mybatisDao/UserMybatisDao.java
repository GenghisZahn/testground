package com.genghiszahn.testground.mybatisDao;

import org.apache.ibatis.annotations.Param;

import com.genghiszahn.testground.dao.UserDao;
import com.genghiszahn.testground.model.User;

public interface UserMybatisDao extends UserDao {

//    @Override
    User retrieveUser(
            @Param("userId") int userId);
    
//    @Override
    User getUserByLoginName(
            @Param("userLoginName") String userLoginName);
    
}
