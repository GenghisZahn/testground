package com.genghiszahn.testground.mybatisDao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.SqlSession;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.transaction.annotation.Transactional;

import com.genghiszahn.testground.dao.CollectionDao;
import com.genghiszahn.testground.model.AccessType;
import com.genghiszahn.testground.model.ItemCollection;

public class CollectionMybatisDao extends SqlSessionDaoSupport implements CollectionDao {

//    @Override
    public List<ItemCollection> getCollectionsForUser(@Param("userId") int userId) {
        return getSqlSession().selectList("getCollectionsForUser", userId);
    }

//    @Override
    @Transactional
    public void createCollection(int userId,
                                 ItemCollection collection) {
        
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("userId", userId);
        paramMap.put("collection", collection);
        
        SqlSession session = getSqlSession();
        session.insert("createCollection", paramMap);
        
        // Create collections_items records
        session.insert("createCollectionItems", paramMap);
        
        // Create accounts record associating collectionId with userId
        paramMap.put("permissionCode", AccessType.READ_WRITE);
        session.insert("createAccount", paramMap);
    }

//    @Override
    public ItemCollection retrieveCollection(int userId,
                                             int collectionId) {
        
        Map<String, Integer> paramMap = new HashMap<String, Integer>();
        paramMap.put("userId", userId);
        paramMap.put("collectionId", collectionId);
        return (ItemCollection) getSqlSession().selectOne("retrieveCollection", paramMap);
    }

//    @Override
    public void updateCollection(int userId,
                                 final ItemCollection collection) {
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("userId", userId);
        paramMap.put("collection", collection);
        
        SqlSession session = getSqlSession();
        session.update("updateCollection", paramMap);
        
        // update collections_items
        paramMap.put("collectionId", collection.getId());
        session.delete("deleteCollectionItems", paramMap);
        if (collection.getItemList() != null && !collection.getItemList().isEmpty()) {
            session.insert("createCollectionItems", paramMap);
        }
        
        // update account access
        if (collection.getAccessType() != null) {
            paramMap.put("permissionCode", collection.getAccessType());
        } else {
            paramMap.put("permissionCode", AccessType.READ_WRITE);
        }
        session.update("updateAccount", paramMap);
    }

//    @Override
    @Transactional
    public void deleteCollection(int userId,
                                 int collectionId) {

        Map<String, Integer> paramMap = new HashMap<String, Integer>();
        paramMap.put("userId", userId);
        paramMap.put("collectionId", collectionId);

        SqlSession session = getSqlSession();

        session.delete("deleteCollectionItems", paramMap);
        session.delete("deleteAccount", paramMap);
        session.delete("deleteCollection", paramMap);
    }

}
