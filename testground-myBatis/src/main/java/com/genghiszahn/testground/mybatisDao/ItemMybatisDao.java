package com.genghiszahn.testground.mybatisDao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.genghiszahn.testground.dao.ItemDao;
import com.genghiszahn.testground.model.Item;

public interface ItemMybatisDao extends ItemDao {

//    @Override
    public void createItem(@Param("item") Item item);
    
//    @Override
    public List<Item> getItemsForUser(@Param("userId") int userId);
}
