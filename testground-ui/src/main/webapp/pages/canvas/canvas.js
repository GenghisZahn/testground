var stage;
var objLayer;
function initCanvas() {
    stage = new Kinetic.Stage({
        container: 'canvasDiv',
        width: 1024,
        height: 768
      });
    
    var gridsize = 64;
    var grid = new Kinetic.Layer();

    // vertical
    for(var x = 0; x <= 1024; x += gridsize) {
        var line = new Kinetic.Line({
            points: [x, 0, x, 768],
            stroke: 'black',
            strokeWidth: 2
        });
        grid.add(line);
    }
    
    // horizontal
    for(var y = 0; y <= 768; y += gridsize) {
        var line = new Kinetic.Line({
            points: [0, y, 1024, y],
            stroke: 'black',
            strokeWidth: 2
        });
        grid.add(line);
    }
    
    objLayer = new Kinetic.Layer();
    
    var box = new Kinetic.Rect({
        x: gridsize,
        y: gridsize,
        width: gridsize,
        height: gridsize,
        fill: '#00D2FF',
        stroke: 'black',
        strokeWidth: 2,
        draggable: true,
        dragBoundFunc: function(pos) {
        	var newX = pos.x < (1024-gridsize) ? pos.x : (1024-gridsize);
            var newY = pos.y < (768-gridsize) ? pos.y : (768-gridsize);
        	newX = newX > 0 ? newX : 0;
            newY = newY > 0 ? newY : 0;
            return {
              x: newX,
              y: newY
            };
          }        
    });

    // add cursor styling
    box.on('mouseover', function() {
        document.body.style.cursor = 'pointer';
    });
    box.on('mouseout', function() {
        document.body.style.cursor = 'default';
    });
    
    box.on('dragend', function(e) {
    	e.target.setX(Math.round(e.target.getX() /gridsize)*gridsize);
    	e.target.setY(Math.round(e.target.getY() /gridsize)*gridsize);
    	objLayer.draw();
    });
    
    objLayer.add(box);
    
    stage.add(grid);
    stage.add(objLayer);
}