package com.genghiszahn.testground.ui;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.genghiszahn.testground.model.User;

/**
 * 
 * @author zahnant
 *
 */
public class UserAuthenticationProvider implements AuthenticationProvider {
    private String loginServiceURL;
    
    public void setLoginServiceURL(String loginServiceURL) {
        this.loginServiceURL = loginServiceURL;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String name = authentication.getName();
        String password = authentication.getCredentials().toString();

        MultiValueMap<String,String> paramMap = new LinkedMultiValueMap<String,String>();
        paramMap.add("userName", name);
        paramMap.add("password", password);
        
        RestTemplate template = new RestTemplate();
        HttpMessageConverter<MultiValueMap<String, ?>> formHttpMessageConverter = new FormHttpMessageConverter();
//        HttpMessageConverter<String> stringHttpMessageConverter = new StringHttpMessageConverter();
        // TODO: use JacksonMessageConverter to map result from string to object
            HttpMessageConverter<Object> jsonHttpMessageConverter = new MappingJackson2HttpMessageConverter();            
        template.setMessageConverters(Arrays.asList(formHttpMessageConverter, jsonHttpMessageConverter));
        
        User resultObj = null;
        try {
            resultObj = template.postForObject(new URI(loginServiceURL), paramMap, User.class);
        } catch (RestClientException | URISyntaxException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        // grant roles
        List<GrantedAuthority> grantedAuths = new ArrayList<GrantedAuthority>();
        grantedAuths.add(new SimpleGrantedAuthority("ROLE_USER"));
//        grantedAuths.add(new SimpleGrantedAuthority("ROLE_ADMIN"));

        if (resultObj != null) {
            System.out.println(resultObj);
            // create auth token
            Authentication auth = new UsernamePasswordAuthenticationToken(name, password, grantedAuths);
            return auth;
        } else {
            return null;
        }
    }
 
    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }
}
