package com.genghiszahn.testground.mongodb;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.genghiszahn.testground.dao.UserDao;
import com.genghiszahn.testground.model.User;

@Repository
public class UserMongoDao implements UserDao {

	private static final String OBJECT_NAME = User.class.getSimpleName();
	
	@Autowired
	private MongoOperations mongoDB;
	

	public User retrieveUser(int userId) {
		return mongoDB.findById(userId, User.class, OBJECT_NAME);
	}

	public User getUserByLoginName(String userLoginName) {
		// TODO Auto-generated method stub
		return null;
	}

}
