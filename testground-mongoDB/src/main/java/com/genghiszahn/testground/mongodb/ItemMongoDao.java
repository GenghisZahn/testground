package com.genghiszahn.testground.mongodb;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.stereotype.Repository;

import com.genghiszahn.testground.dao.ItemDao;
import com.genghiszahn.testground.model.Item;

@Repository
public class ItemMongoDao implements ItemDao {

	@Autowired
	private MongoOperations mongoDB;
	
	public void createItem(Item item) {
		mongoDB.insert(item, Item.class.getSimpleName());
	}

	public List<Item> getItemsForUser(int userId) {
		// TODO Auto-generated method stub
		return null;
	}

}
