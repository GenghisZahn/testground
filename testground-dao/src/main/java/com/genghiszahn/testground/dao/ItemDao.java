package com.genghiszahn.testground.dao;

import java.util.List;

import com.genghiszahn.testground.model.Item;

public interface ItemDao {
    
    /**
     * 
     * @param item
     */
    void createItem(Item item);
    
    /**
     * 
     * @param userId
     * @return
     */
    List<Item> getItemsForUser(int userId);
}
