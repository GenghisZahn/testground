package com.genghiszahn.testground.dao;

import com.genghiszahn.testground.model.User;

public interface UserDao {
    
    /**
     * 
     * @param userId
     * @return
     */
    User retrieveUser(int userId);
    
    /**
     * 
     * @param userLoginName
     * @return
     */
    User getUserByLoginName(String userLoginName);
}
