package com.genghiszahn.testground.dao;


import java.util.List;

import com.genghiszahn.testground.model.ItemCollection;

public interface CollectionDao {
    
    List<ItemCollection> getCollectionsForUser(int userId);
    
    // CRUD operations
    void createCollection(int userId, ItemCollection collection);
    ItemCollection retrieveCollection(int userId, int collectionId);
    void updateCollection(int userId, ItemCollection collection);
    void deleteCollection(int userId, int collectionId);
}
